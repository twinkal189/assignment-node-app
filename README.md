This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Application Functionality

assignment application with node js and quorra js.
-For setting up database in to mongo please follow below step.
-Open mongodb compass.
-create one database with name "test".
-create 4 collection using these name(answers,classes,assignments,students).
-import data for each collection from folder DB where JSON files are available.
-Now we are good to go.


### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:5000](http://localhost:5000) to hit api.


### Application Snapshot
![Application snapshot](screenshots/loginapi.PNG)
![Application snapshot](screenshots/assignmentlistapi.PNG)
![Application snapshot](screenshots/assignmentdetailapi.PNG)
![Application snapshot](screenshots/createanswerapi.PNG)