const fs = require('fs');
const jwt = require('jsonwebtoken');
var auth = {

    //generate token
    generateToken: (id) => {
        //to generate the token
        // PAYLOAD
        var payload = {

            id: id
        };
        // PRIVATE and PUBLIC key
        var privateKEY = fs.readFileSync('./private.key', 'utf8');
        var publicKEY = fs.readFileSync('./public.key', 'utf8');
        var i = 'Mysoft corp';          // Issuer
        var s = 'some@user.com';        // Subject
        var a = 'http://mysoftcorp.in'; // Audience
        // SIGNING OPTIONS
        var signOptions = {
            issuer: i,
            subject: s,
            audience: a,
            //expiresIn: "365d",
            algorithm: "RS256",

        };
        var token = jwt.sign(payload, privateKEY, signOptions);
        console.log("JWT Token - " + token)

        return token

        //end
    },
    //to verify the sent token from the client
    verifyToken: (token, res,next) => {
        // PAYLOAD

        // PRIVATE and PUBLIC key
        var publicKEY = fs.readFileSync('./public.key', 'utf8');
        var i = 'Mysoft corp';          // Issuer
        var s = 'some@user.com';        // Subject
        var a = 'http://mysoftcorp.in'; // Audience
        var verifyOptions = {
            issuer: i,
            subject: s,
            audience: a,
            //expiresIn: "365d",
            algorithm: ["RS256"],

        };
        try {
            var legit = jwt.verify(token, publicKEY, verifyOptions);
            console.log("\nJWT verification result: " + JSON.stringify(legit));

            next()

        }
        catch (err) {
            console.log(err)
            res.statusCode = 401;
            res.send({ status: 0, message: 'Token expired.' });
        }

        //end
    },
}
module.exports = auth;