var multer = require('multer');
const storage = multer.diskStorage({
    
    destination : function(req,file,callback){
        callback(null, 'public/upload');
    },
    filename: function(req,file,callback){
        var ext=file.originalname.split('.')
        callback(null, Date.now() + "."+ext[ext.length-1]);
    }
});

var parser = multer({ storage : storage}).any();

function MultipartParserMiddleware(app, next){
    // Quorra app instance
    this.__app = app;

    // Next middleware to execute
    this.__next = next;
}

MultipartParserMiddleware.prototype.handle = function(request, response) {
    var self = this;

    // handle requests here
    parser(request, response, function (err) {
        if (err) {
            throw err;
        }
        // console.log(request.files[0])
        //console.log('aaaa')//.originalname.split('.')[1])
        // Everything went fine
        self.__next.handle(request, response);
    });
};
module.exports = MultipartParserMiddleware;