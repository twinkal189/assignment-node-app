
var Route = App.router;
App.middleware(require('./middlewares/MultipartParserMiddleware'));
Route.group({ 'before': 'auth' }, function () {});

//#region API

Route.post('login', 'AdminController@getUser');
Route.post('createUser', 'AdminController@createUser');
Route.post('getAssignmentList', 'AdminController@getAssignmentList');
Route.post('imageUpload', 'AdminController@imageUpload');
Route.post('getAssignmentDetail', 'AdminController@getAssignmentDetail');
Route.post('createAnswer', 'AdminController@createAnswer');

Route.controller('users', 'HomeController');
Route.controller('admin', 'AdminController');
