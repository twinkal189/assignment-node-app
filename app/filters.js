var App = require('positron');
let jwt = require('jsonwebtoken');
var Route = App.router;

//let token=require('../app/config/auth')
const fs = require('fs');



/*
 |--------------------------------------------------------------------------
 | Application & Route Filters
 |--------------------------------------------------------------------------
 |
 | Below you will find the "before" event for the application
 | which may be used to do any work before a request into your
 | application. Here you may also register your custom route filters.
 |
 */
App.before(function (request, result, next) {
  next();
});

/*
 |--------------------------------------------------------------------------
 | Authentication Filters
 |--------------------------------------------------------------------------
 |
 | The following filters are used to verify that the user of the current
 | session is logged into this application. The "basic" filter easily
 | integrates HTTP Basic authentication for quick, simple checking.
 |
 */

Route.filter('auth', function (req, res, next) {
  if(req.headers.auth) {
  token = req.headers.auth
  if (token) {
    // PRIVATE and PUBLIC key
    var publicKEY = fs.readFileSync('./public.key', 'utf8');
    var i = 'Mysoft corp';          // Issuer
    var s = 'some@user.com';        // Subject
    var a = 'http://mysoftcorp.in'; // Audience
    var verifyOptions = {
      issuer: i,
      subject: s,
      audience: a,
      //expiresIn: "365d",
      algorithm: ["RS256"],

    };
    try {
      var legit = jwt.verify(token, publicKEY, verifyOptions);
            req.body.UserID = legit.id;
      console.log("\nJWT verification result: " + JSON.stringify(legit));
      next();

    }
    catch (err) {
      console.log(err)
      res.statusCode = 401;
      res.send({ status: 0, message: 'Token expired.' });
    }

  }
  else {
    return res.json({
      success: false,
      message: 'Auth token is not supplied'
    });
  }
} else {
  next();
}
});

Route.filter('auth.basic', function (request, response, next) {
  request.auth.basic(function (result) {
    if (result) {
      next();
    } else {
      var error = new Error('Invalid credentials');

      error.status = 401;
      response.setHeader('WWW-Authenticate', 'Basic');
      response.abort(error);
    }
  });
});



//#region  admin token
Route.filter('auth1', function (req, res, next) {
  if(req.headers.auth) {
  token = req.headers.auth
  if (token) {
    // PRIVATE and PUBLIC key
    var publicKEY = fs.readFileSync('./public.key', 'utf8');
    var i = 'Mysoft corp';          // Issuer
    var s = 'some@user.com';        // Subject
    var a = 'http://mysoftcorp.in'; // Audience
    var verifyOptions = {
      issuer: i,
      subject: s,
      audience: a,
      //expiresIn: "365d",
      algorithm: ["RS256"],

    };
    try {
      var legit = jwt.verify(token, publicKEY, verifyOptions);
            req.body.UserID = legit.id;
      console.log("\nJWT verification result: " + JSON.stringify(legit));
      next();

    }
    catch (err) {
      console.log(err)
      res.statusCode = 401;
      res.send({ status: 0, message: 'Token expired.' });
    }

  }
  else {
    return res.json({
      success: false,
      message: 'Auth token is not supplied'
    });
  }
} else {
  next();
}
});

Route.filter('auth1.basic', function (request, response, next) {
  request.auth.basic(function (result) {
    if (result) {
      next();
    } else {
      var error = new Error('Invalid credentials');

      error.status = 401;
      response.setHeader('WWW-Authenticate', 'Basic');
      response.abort(error);
    }
  });
});

//#endregion




/*
 |--------------------------------------------------------------------------
 | Guest Filter
 |--------------------------------------------------------------------------
 |
 | The "guest" filter is the counterpart of the authentication filters as
 | it simply checks that the current user is not logged in. A redirect
 | response will be issued if they are, which you may freely change.
 |
 */

Route.filter('guest', function (request, response, next) {
  request.auth.check(function (result) {
    if (result) {
      response.redirect('/');
    } else {
      next();
    }
  })
});

/*
 |--------------------------------------------------------------------------
 | CSRF Protection Filter
 |--------------------------------------------------------------------------
 |
 | The CSRF filter is responsible for protecting your application against
 | cross-site request forgery attacks. If this special token in a user
 | session does not match the one given in this request, we'll bail.
 |
 */

Route.filter('csrf', function (request, response, next) {
  if (request.session.getToken() !== request.input.get('_token')) {
    throw new Error('Token mismatch');
  } else {
    next();
  }
});
