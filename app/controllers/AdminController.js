const nodeMailer = require("nodemailer");
var BaseController = require("./BaseController");
var AdminController = BaseController.extend(function () {
  this.getUser = function (req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE");
    res.header(
      "Access-Control-Allow-Headers",
      "Content-Type:application/x-www-form-urlencoded"
    );
    res.header("Content-Type", "application/x-www-form-urlencoded");
    Student.findOne({ email: req.body.email, password: req.body.password }).populate('classId').exec(function (err, data) {
      let result = {}
      if (data) {
        result.status = 1
        result.message = "Login successfully"
        result.data = data
        return res.json(result)
      } else {
        result.status = 0
        result.message = "Invalid user"
        result.data = {}
        return res.json(result)

      }

    });
  };
  this.createUser = function (req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE");
    res.header(
      "Access-Control-Allow-Headers",
      "Content-Type:application/x-www-form-urlencoded"
    );
    res.header("Content-Type", "application/x-www-form-urlencoded");

    let response = {};
    var myObj = {
      name: req.body.name,
      email: req.body.email,
      password: req.body.password,
    };
    Student.create(myObj).exec(function (err, data) {
      if (data) {
        response.status = 1;
        response.message = "student added.";
        response.data = data;
        return res.json(response);
      } else {
        response.status = 0;
        response.message = "Admin not added";
        response.data = {};
        return res.json(response);
      }
    });
  }
  this.getAssignmentList = function (req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE");
    res.header(
      "Access-Control-Allow-Headers",
      "Content-Type:application/x-www-form-urlencoded"
    );
    res.header("Content-Type", "application/x-www-form-urlencoded");
    Student.findOne({ id: req.body.studentId }).populate('classId').exec(function (err, student) {
      if (student) {
        if (student.classId) {
          if (req.body.search && req.body.search !== "") {
            Assignment.find({ classId: student.classId.id ,isActive:"Y"}).where({ subject: { contains: req.body.search } })
              .exec(function (err, data) {
                let result = {}
                if (data) {
                  result.status = 1
                  result.message = "Fetch data successfully"
                  result.data = data
                  return res.json(result)
                } else {
                  result.status = 0
                  result.message = "Data not found"
                  result.data = {}
                  return res.json(result)
                }
              });

          } else {
            Assignment.find({ classId: student.classId.id }).exec(function (err, data) {
              let result = {}
              if (data) {
                result.status = 1
                result.message = "Fetch data successfully"
                result.data = data
                return res.json(result)
              } else {
                result.status = 0
                result.message = "Data not found"
                result.data = {}
                return res.json(result)
              }
            });

          }
        }
        else {
          result.status = 0
          result.message = "Data not found"
          result.data = {}
          return res.json(result)
        }
      } else {
        result.status = 0
        result.message = "Data not found"
        result.data = {}
        return res.json(result)
      }
    })
  };
  this.imageUpload = function (req, res) {
    res.header('Access-Control-Allow-Origin', "*");
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type:application/x-www-form-urlencoded');
    res.header('Content-Type', 'application/x-www-form-urlencoded');
    var response = {}
    if (req.files[0]) {
      response.status = 1
      response.message = "image upload"
      response.path = 'http://localhost:5000/upload/' + req.files[0].filename
      response.files = req.files[0]
      res.json(response)
    }
    else {
      response.status = 0
      response.message = "image not uploaded"
      res.json(response)
    }
  }
  this.getAssignmentDetail = function (req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE");
    res.header(
      "Access-Control-Allow-Headers",
      "Content-Type:application/x-www-form-urlencoded"
    );
    res.header("Content-Type", "application/x-www-form-urlencoded");
    Assignment.findOne({ id: req.body.id }).exec(function (err, data) {
      let result = {}
      if (data) {
        Answer.find({ studentId: req.body.studentId, assignmentId: data.id }).exec(function (err, answer) {
          result.status = 1
          result.message = "Fetch data successfully"
          result.data = data
          result.answer = answer
          return res.json(result)
        })
      } else {
        result.status = 0
        result.message = "Data not found"
        result.data = {}
        return res.json(result)
      }
    });
  };
  this.createAnswer = function (req, res) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE");
    res.header(
      "Access-Control-Allow-Headers",
      "Content-Type:application/x-www-form-urlencoded"
    );
    res.header("Content-Type", "application/x-www-form-urlencoded");
    myObj={
      studentId:req.body.studentId,
      assignmentId:req.body.assignmentId,
      answerFile:req.body.answerFile
    }
    Answer.create(myObj).exec(function (err, data) {
      let result = {}
      if (data) {
          result.status = 1
          result.message = "Answer created successfully"
          result.data = data
          return res.json(result)
      } else {
        result.status = 0
        result.message = "Data not added"
        result.data = {}
        return res.json(result)
      }
    });
  };
});

module.exports = AdminController;
