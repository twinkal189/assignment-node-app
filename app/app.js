const https = require('https');
var configuration = require('./app-config');
var path = require('path');
const fs = require('fs');
const jwt = require('jsonwebtoken');
const options = {
  key: fs.readFileSync('key.pem'),
  cert: fs.readFileSync('cert.pem')
};
var mongoose = require('mongoose');
mongoose.connect(`${configuration.db.aws.mongoDB.URI}`, { useNewUrlParser: true, useUnifiedTopology: true })
  .then(() => {
    https.createServer(options, app).listen(443);
  })
  .catch((err) => {
    console.log(err)
  })

