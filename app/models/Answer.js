
var Answer = {
    attributes: {
        studentId: {
           model:'student'
        },
        assignmentId: {
            model:'assignment'
        },
        answerFile: {
            required: false,
            type: 'string'
        },
    },

    // migrate: 'drop',

    /**
     * Get model remember token field name.
     * Used by quorra auth service.
     *
     * @returns {string}
     */
    getRememberTokenName: function () {
        return 'remember_token';
    }
};

module.exports = Answer;

