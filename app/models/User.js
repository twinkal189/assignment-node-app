
var User = {
    attributes: {
        //basic sign up
        email: {
            required: true,
            type: 'string'
        },
        password: {
            required: false,
            type: 'string'
        },
                // remember_token: 'string'
    },

    // migrate: 'drop',

    /**
     * Get model remember token field name.
     * Used by quorra auth service.
     *
     * @returns {string}
     */
    getRememberTokenName: function () {
        return 'remember_token';
    }
};

module.exports = User;

