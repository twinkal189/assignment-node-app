var Assignment = {
    attributes: {
        title: {
            required: true,
            type: 'string'
        },
        description: {
            required: false,
            type: 'string'
        },
        classId: {
            model: 'class'
        },
        subject: {
            required: false,
            type: 'string'
        },
        totalMark: {
            required: false,
            type: 'string'
        },
        questionPaper: {
            required: false,
            type: 'string'
        },
        isActive: {
            required: false,
            type: 'string'
        },
    },

    // migrate: 'drop',
    /**
     * Get model remember token field name.
     * Used by quorra auth service.
     *
     * @returns {string}
     */
    getRememberTokenName: function () {
        return 'remember_token';
    }
};

module.exports = Assignment;

