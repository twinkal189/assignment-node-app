
var Student = {
    attributes: {
        name: {
            required: false,
            type: 'string'
        },
        email:{
            required: true,
            type: 'string'
        },
        password:{
            required: true,
            type: 'string'
        },
        classId: {
            model:'class'
        },
    },
    // migrate: 'drop',
    /**
     * Get model remember token field name.
     * Used by quorra auth service.
     *
     * @returns {string}
     */
    getRememberTokenName: function () {
        return 'remember_token';
    }
};

module.exports = Student;

