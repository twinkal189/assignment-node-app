
var Class = {
    attributes: {
        name: {
            required: true,
            type: 'string'
        },
    },

    // migrate: 'drop',

    /**
     * Get model remember token field name.
     * Used by quorra auth service.
     *
     * @returns {string}
     */
    getRememberTokenName: function () {
        return 'remember_token';
    }
};

module.exports = Class;

