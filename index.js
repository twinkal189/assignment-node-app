// const cron = require('node-cron');
/*
 |--------------------------------------------------------------------------
 | Start the Engine
 |--------------------------------------------------------------------------
 |
 | We need to enlighten  Node.js development, so let's start the engine,
 | turn on the lights. This bootstraps the framework and gets it ready for use,
 | then it will load up this application so that we can run it and send
 | the responses back to the browser and delight these users.
 |
 */

require('./bootstrap/start')(function (app) {

    /*
     |--------------------------------------------------------------------------
     | Listen for requests
     |--------------------------------------------------------------------------
     |
     | Once we have the application, we can simply call the listen method,
     | which will start to listen for user requests.
     |
     */

    // cron.schedule('1 * * * * *', function () {
    //     //graphQlResolvers.pligrim();
    //     UserController.StoryTime()
    //     console.log('te')

    // });
    // var fedexAPI = require('fedex');
 
    // var fedex = new fedexAPI({
    //   environment: 'sandbox', // or live
    //   debug: true,
    //   key: 'KEY',
    //   password: 'DEVPASSWORD',
    //   account_number: 'ACCOUNT#',
    //   meter_number: 'METER#',
    //   imperial: true // set to false for metric
    // });

    // require('./app/controllers/UserController')(function (app) {
    // cron.schedule('1 * * * * *', function () {
    //     UserController.StoryTime()
    //     console.log('te')

    // });
    // })
    app.listen();
});